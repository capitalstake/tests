# Frontend UI Engineer Test (TypeScript)

This tasks is designed to help you work with frontend technologies we use at CapitalStake. We don't expect you to be fluent in language and concepts so take your time to familiarize yourself while attempting this task.

You will have 2 weeks to complete this task.

## Technologies

[TypeScript](https://www.typescriptlang.org/)

[Webpack](https://webpack.js.org/)

[NodeJS](https://nodejs.org/en/)


## Task

Create a Autocomplete UI component that takes DOM element ID and array of strings as parameter. DOM element should be of type `<input />`. 

You can use following source code to create your own component:
[w3schools.com - How To - Autocomplete](https://www.w3schools.com/howto/howto_js_autocomplete.asp)

Your task is to:

- Convert it into a TypeScript class
- Class should have functionality divided into methods and properties
- TypeScript class should be contained in a separate file `Autocomplete.ts`
- You can copy same CSS, HTML from the above tutorial

### Project Structure

Since browser can't interpret TypeScript directly, you will use Webpack to convert TypeScript source code into JavaScript.

Your project structure should look like:

```
project-folder
  |- package.json
  |- webpack.config.js
  |- tsconfig.json
  |- /dist
      |- index.html
      |- bundle.js
  |- /src
      |- index.js
      |- Autocomplete.js
```

`webpack.config.js` will contain webpack configuration to convert `/src` code into `/dist/bundle.js` file.

You can place your HTML and CSS code inside `/dist/index.html`.

[Setup TypeScript & Webpack](https://webpack.js.org/guides/typescript/)

## Submission

You are required to create a public repository on **GitHub.com**, place your project source code into that repository and share the public repository URL with us.

## Evaluation

We will evaluate your submission on following basis:

- Task Completion
- Project Setup
- Code Organization 
- Data Structures

## Learning Resource

[TypeScript in 5 minutes](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)

[Webpack & TypeScript Setup #1 - Introduction](https://www.youtube.com/watch?v=sOUhEJeJ-kI)